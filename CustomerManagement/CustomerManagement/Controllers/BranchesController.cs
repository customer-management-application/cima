﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CustomerManagement.Models;
using CustomerManagement.Interface;
using CustomerManagement.DTO;

namespace CustomerManagement.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BranchesController : ControllerBase
    {
        private IBranch service;
        public BranchesController(IBranch service)
        {
            this.service = service;
        }

        [Route("Get-branches")]
        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            ResponseAPI<List<Branch>> branches = new ResponseAPI<List<Branch>>();
            try
            {
                branches.Data = await this.service.GetList();
                return Ok(branches);
            }
            catch (Exception ex)
            {
                branches.Message = ex.Message;
                return BadRequest(branches);
            }
        }

        [Route("Create")]
        [HttpPost]
        public async Task<IActionResult> Insert(BranchCreateDTO dto)
        {
            ResponseAPI<List<Branch>> branches = new ResponseAPI<List<Branch>>();
            try
            {
                branches.Data = await this.service.AddNewBranch(dto); ;
                return Ok(branches);
            }
            catch (Exception e)
            {
                branches.Message = e.Message;
                return BadRequest(branches);
            }
        }

        [Route("Delete")]
        [HttpDelete]
        public async Task<IActionResult> Delete(int id)
        {
            ResponseAPI<List<Branch>> branches = new ResponseAPI<List<Branch>>();
            try
            {
                branches.Data = await this.service.DeleteById(id);
                return Ok(branches);
            }
            catch (Exception e)
            {
                branches.Message = e.Message;
                return BadRequest(branches);
            }

        }

        [Route("Update")]
        [HttpPut]
        public async Task<IActionResult> Update(BranchDTO dto)
        {
            ResponseAPI<List<Branch>> branches = new ResponseAPI<List<Branch>>();
            try
            {
                branches.Data = await this.service.Edit(dto);
                return Ok(branches);
            }
            catch (Exception e)
            {
                branches.Message = e.Message;
                return BadRequest(branches);
            }

        }

    }
}