﻿using CustomerManagement.DTO;
using CustomerManagement.Models;

namespace CustomerManagement.Interface
{
    public interface IBranch
    {
        Task<List<Branch>> GetList();
        Task<Branch> AddNewBranch(BranchCreateDTO newBranch);
        Task<Branch> DeleteById(int BranchId);
        Task<Branch> Edit(BranchDTO updateBranch);

    }
}
