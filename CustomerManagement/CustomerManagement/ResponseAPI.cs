﻿using System.Runtime.Serialization.Json;

namespace CustomerManagement
{
    public class ResponseAPI<T>
    {
        public string Message { get; set; }

        public bool _isIgnoreNullData;
        private object _data;
        public object Data
        {
            get; set;
        }


        public ResponseAPI(bool isIgnoreNullData = true)
        {
            this._isIgnoreNullData = isIgnoreNullData;
        }
    }
}
