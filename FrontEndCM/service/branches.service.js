const endpoint = "https://localhost:7293/api/Branches";
const getBranches = async () => {
    const response = await fetch(`${endpoint}/Get-branches`);
    if (!response.ok) {
      throw new Error("Invalid Request");
    } else {
      return response.json();
    }
  };

  const createBranch = async (model) => {

    const response = await fetch(`${endpoint}/Create`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: model ? JSON.stringify(model) : undefined,
    });
    if (!response.ok) {
      throw new Error("Invalid Request");
    } else {
      return response.json();
    }
  };
  
  export {
    getBranches,
    createBranch, 
  };