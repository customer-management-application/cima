USE [CustomerManagement]
GO
/****** Object:  Table [dbo].[Branch]    Script Date: 9/18/2023 10:01:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Branch](
	[BranchId] [int] NOT NULL,
	[Name] [nvarchar](50) NULL,
	[Address] [nvarchar](50) NULL,
	[City] [nvarchar](50) NULL,
	[State] [nvarchar](50) NULL,
	[ZipCode] [nvarchar](50) NULL,
 CONSTRAINT [PK_Branch] PRIMARY KEY CLUSTERED 
(
	[BranchId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Branch] ([BranchId], [Name], [Address], [City], [State], [ZipCode]) VALUES (0, N'string', N'string', N'string', N'DeActive', N'string')
INSERT [dbo].[Branch] ([BranchId], [Name], [Address], [City], [State], [ZipCode]) VALUES (1, N'newbranch', N'9/12 Nguyen Khuyen', N'Bien Hoa', N'Active', N'918427')
GO
