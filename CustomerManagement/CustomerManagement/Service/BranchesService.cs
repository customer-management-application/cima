﻿using CustomerManagement.DTO;
using CustomerManagement.Interface;
using CustomerManagement.Models;
using Microsoft.EntityFrameworkCore;

namespace CustomerManagement.Service
{
    public class BranchesService : IBranch
    {

        private readonly CustomerManagementContext _context;
        public BranchesService(CustomerManagementContext context)
        {
            _context = context;
        }


        public async Task<List<Branch>> GetList()
        {
            try
            {
                var check = _context.Branch.ToList();
                if (check != null)
                {
                    return check;
                }
                return null;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<Branch> AddNewBranch(BranchCreateDTO newBranch)
        {
            try
            {
                var branch = new Branch();
                branch.BranchId = newBranch.BranchId;
                branch.Name = newBranch.Name;
                branch.Address = newBranch.Address;
                branch.City = newBranch.City;
                branch.State = "Active";
                branch.ZipCode = newBranch.ZipCode;

                await this._context.Branch.AddAsync(branch);
                await this._context.SaveChangesAsync();
                return branch;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<Branch> DeleteById(int branchId)
        {
            try
            {
                var branch = await this._context.Branch.Where(x => x.BranchId.Equals(branchId)).FirstOrDefaultAsync();
                if (branch != null)
                {

                    branch.State = "DeActive";
                    this._context.Branch.Update(branch);
                    await this._context.SaveChangesAsync();

                    return branch;
                }
                else
                {
                    throw new Exception("Id is not exist!!");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<Branch> Edit(BranchDTO updateBranch)
        {
            try
            {
                var check = await this._context.Branch.Where(x => x.BranchId.Equals(updateBranch.BranchId)).FirstOrDefaultAsync();
                if (updateBranch.Name != null)
                {
                    check.Name = updateBranch.Name;
                }
                if (updateBranch.Address != null)
                {
                    check.Address = updateBranch.Address;
                }
                if (updateBranch.City != null)
                {
                    check.City = updateBranch.City;
                }
                if (updateBranch.ZipCode != null)
                {
                    check.ZipCode = updateBranch.ZipCode;
                }
                if (updateBranch.State != null)
                {
                    check.State = updateBranch.State;
                }

                this._context.Branch.Update(check);
                await this._context.SaveChangesAsync();

                return check;

            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}
