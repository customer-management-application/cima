// branches.js

import { getBranches, createBranch } from "./service/branches.service.js";

$(document).ready(function () {
  loadBranchList();
  setupCreateBranchButton();
});



async function loadBranchList() {
  try {
    getBranches().then(response => {
    const branches = response.data   
    displayBranchList(branches);
});
  } catch (error) {
    console.error('An error occurred while fetching branches: ' + error.message);
    // Handle the error here (e.g., display an error message)
  }
}

function displayBranchList(branches) {
  const branchTableBody = document.getElementById("branch-table-body");

  if (Array.isArray(branches)) {
    // Clear the table body
    branchTableBody.innerHTML = "";

    // Iterate through branches and populate the table
    branches.forEach((branch) => {
      const row = document.createElement("tr");
      row.innerHTML = `
        <td>${branch.branchId}</td>
        <td>${branch.name}</td>
        <td>${branch.address}</td>
        <td>${branch.city}</td>
        <td>${branch.state}</td>
        <td>${branch.zipCode}</td>
      `;

      branchTableBody.appendChild(row);
    });
  } else {
    // Handle the case where branches is not an array (e.g., no branches found)
    branchTableBody.innerHTML = "<tr><td colspan='7'>No branches found.</td></tr>";
  }
}

function setupCreateBranchButton() {
  const createBranchButton = document.getElementById("create-branch-btn");

  if (createBranchButton) {
    createBranchButton.addEventListener("click", () => {
      Swal.fire({
        title: "Create Branch",
        html: `
          <form id="branch-form">
            <div class="mb-3">
              <label for="branchId" class="form-label">ID</label>
              <input type="text" class="form-control" id="branchId" name="branchId" required>
            </div>
            <div class="mb-3">
              <label for="name" class="form-label">Name</label>
              <input type="text" class="form-control" id="name" name="name" required>
            </div>
            <div class="mb-3">
              <label for="address" class="form-label">Address</label>
              <input type="text" class="form-control" id="address" name="address" required>
            </div>
            <div class="mb-3">
              <label for="city" class="form-label">City</label>
              <input type="text" class="form-control" id="city" name="city" required>
            </div>
            <div class="mb-3">
              <label for="zipCode" class="form-label">Zip Code</label>
              <input type="text" class="form-control" id="zipCode" name="zipCode" required>
            </div>
          </form>
        `,
        showCancelButton: true,
        confirmButtonText: "Create",
        preConfirm: () => {
          // Handle form submission here
          const formData = new FormData(document.getElementById("branch-form"));
          const branchData = {};
          formData.forEach((value, key) => {
            branchData[key] = value;
          });

          // Call your createBranch function here with branchData
          createBranch(branchData)
            .then((response) => {
              // Handle success or error
              if (response && response.data) {
                Swal.fire("Success!", "Branch created successfully.", "success");
                // Reload or update the branch list here
              } else {
                Swal.fire("Error!", "Failed to create branch.", "error");
              }
            })
            .catch((error) => {
              Swal.fire("Error!", "Failed to create branch.", "error");
              console.error("An error occurred while creating a branch: " + error);
            });
        },
      });
    });
  }
}

// Call the setupCreateBranchButton function when the DOM is ready
document.addEventListener("DOMContentLoaded", function () {
  setupCreateBranchButton();
  // Other code
});